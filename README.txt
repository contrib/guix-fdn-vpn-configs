== Status ==
While it works, it has a big security issue: this configuration is
prone to man in the middle attacks.

This is because Guix doesn't support (yet) the "verify-x509-name
open.fdn.fr name" OpenVPN configuration directive.

So we need to either need add support for that in Guix or re-make the
ca certificate in a way that is compatible with "remote-cert-tls
server".

In addition, after boot we need to manually resize the rootfs with
resize2fs to have a bit of space for updates.

In addition we used a fixed revision for Guix to make it work as
otherwise unset would not be defined.

== Usage ==

To build an image, after installing guix and updating it, you need to:

* Create an ed25519 ssh key (if you don't already have one) and copy
  (or symlink) the public key (id_ed25519.pub) to the openvpn-openbar
  directory.

* run 'make' in the openvpn-openbar directory and it'll create a
  system.img file in it.

After that you can for instance boot the system.img with libvirt qemu
and virt-manager.

== License ==
The ca.crt file is licensed is under the CC-BY-SA 3.0 license (it
comes from the FDN wiki that comes from the old wiki that is under
that license).

Everything else in this repository is licensed under the GPLv3 or
later.
