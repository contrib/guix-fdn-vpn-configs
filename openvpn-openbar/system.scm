;;; Copyright © Guix documentation authors
;;; Copyright © 2022 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(use-modules
 (gnu)
 (gnu machine)
 (gnu machine ssh)
 (gnu services networking)
 (gnu services ssh)
 (gnu services vpn)
 (gnu packages admin)
 (gnu packages bootloaders)
 (guix build-system copy)
 ((guix licenses) #:prefix license:)
 (guix packages)
 (guix store))

(define fdn-openvpn-openbar-ca
  (package
   (name "fdn-openvpn-open-ca")
   (version "1.0")
   (source (local-file "ca.crt"))
   (build-system copy-build-system)
   (synopsis "CA certificate for the \"openbar\" FDN VPN")
   (description "CA certificate for the \"openbar\" FDN VPN")
   (home-page "https://git.fdn.fr/fdn-public/wiki/-/blob/master/VPN/doc.md")
   (license license:cc-by-sa3.0)))

(define fdn-openvpn-openbar-auth-user-pass
  (package
   (name "fdn-openvpn-open-auth-user-pass")
   (version "1.0")
   (source (local-file "auth-user-pass.conf"))
   (build-system copy-build-system)
   (synopsis "Username and passwords for the \"openbar\" FDN VPN")
   (description "Username and passwords for the \"openbar\" FDN VPN")
   (home-page "https://git.fdn.fr/fdn-public/wiki/-/blob/master/VPN/doc.md")
   (license license:cc-by-sa3.0)))

(operating-system
 (host-name "base-server")
 (timezone "Europe/Paris")
 (bootloader (bootloader-configuration
              (bootloader grub-bootloader)
              (targets '("/dev/vda"))
              (terminal-outputs '(console))))
 (file-systems (cons (file-system (mount-point "/")
                                  (device "/dev/vda1")
                                  (type "ext4"))
                     %base-file-systems))
 (packages (append (list
                    fdn-openvpn-openbar-ca
                    fdn-openvpn-openbar-auth-user-pass
                    htop
                    nmon)
                   %base-packages))
 (services
  (append
   (list
    (service dhcp-client-service-type)
    (service openssh-service-type
             (openssh-configuration
              (port-number 22)
              (permit-root-login #t)
              (password-authentication? #f)
              (challenge-response-authentication? #f)
              (authorized-keys
               `(("root", (local-file "id_ed25519.pub"))))))
    (openvpn-client-service
     #:config
     (openvpn-client-configuration
      (cert 'unset)
      (key 'unset)
      (ca
       (string-append
        (with-store store (package-output store fdn-openvpn-openbar-ca))
        "/ca.crt"))
      (persist-key? #t)
      (verbosity 3)
      (comp-lzo? #f)
      (verify-key-usage? #f)
      (auth-user-pass
       (string-append
        (with-store store
                    (package-output store fdn-openvpn-openbar-auth-user-pass))
        "/auth-user-pass.conf"))
      (remote
       (list
        (openvpn-remote-configuration (name "open.fdn.fr")))))))
   %base-services)))
